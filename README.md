Line Sticker Downloader
========================================

Just a simple tool to download stickers from Line Store.

![Screenshot](https://bitbucket.org/rakisaionji/linestickerdownloader/raw/b8433cf555d53dbcbd52dcb56002458bc0606b80/References/MainForm.png)

Usage 
----------------------------------------

1. Type in the sticker ID or copy and paste the link of the sticker.

2. Press `[Get Metadata]` and wait for information fetching from server.

3. Adjust your options and preferences then press `[Start Download]` button.

4. Wait until the download finished. Your downloaded files will be stored in the `Sticker/` folder.
