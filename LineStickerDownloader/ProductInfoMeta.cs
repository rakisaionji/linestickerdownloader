﻿using System.Collections.Generic;
using System.Collections.Specialized;

namespace LineStickerDownloader
{
    // [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class LineStorePrice
    {
        public string Country { get; set; }
        public string Currency { get; set; }
        public string Symbol { get; set; }
        public double Price { get; set; }
    }

    // [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class LineStickerMeta
    {
        public int Id { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }

    // [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class ProductInfoMeta
    {
        public int PackageId { get; set; }
        public bool OnSale { get; set; }
        public int ValidDays { get; set; }
        public OrderedDictionary Title { get; set; }
        public OrderedDictionary Author { get; set; }
        public List<LineStorePrice> Price { get; set; }
        public List<LineStickerMeta> Stickers { get; set; }
        public bool HasAnimation { get; set; }
        public bool HasSound { get; set; }
        public string StickerResourceType { get; set; }
    }

}
