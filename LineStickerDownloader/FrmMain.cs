﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace LineStickerDownloader
{
    public partial class FrmMain : Form
    {
        #region " Predefined Strings "

        private const string STORE_URL = @"^(?:http[s]?:\/\/+store.line.me\/+stickershop\/+product\/+(\d+)\/+.*|line:\/\/+shop\/+detail\/+(\d+))\/*|\/*(\d+)\/+$";
        private const string META_URL = @"http://dl.stickershop.line.naver.jp/products/0/0/1/{1}/{0}/productInfo.meta";
        private const string SOLO_URL_1 = @"http://dl.stickershop.line.naver.jp/products/0/0/1/{1}/{0}/stickers/{2}.png";
        private const string SOLO_URL_2 = @"http://dl.stickershop.line.naver.jp/products/0/0/1/{1}/{0}/animation/{2}.png";
        private const string PACK_URL_1 = @"http://dl.stickershop.line.naver.jp/products/0/0/1/{1}/{0}/stickers{2}.zip";
        private const string PACK_URL_2 = @"http://dl.stickershop.line.naver.jp/products/0/0/1/{1}/{0}/stickerpack{2}.zip";

        #endregion

        #region " Local Variables "

        private ProductInfoMeta CurrentPackage { get; set; }

        #endregion

        #region " Functions and Procedures "

        private int GetPackageId(string url)
        {
            try
            {
                int pack_id;
                MatchCollection matches = Regex.Matches(url + "/", STORE_URL);
                foreach (Match match in matches)
                {
                    foreach (Group group in match.Groups)
                    {
                        if (Int32.TryParse(group.Value, out pack_id))
                        {
                            if (pack_id > 0) return pack_id;
                        }
                    }
                }
            }
            catch (Exception) { }
            return -1;
        }

        private ProductInfoMeta GetPackMeta(string platform, int pack_id)
        {
            try
            {
                string pack_meta = "";
                using (WebClient wc = new WebClient())
                {
                    wc.Encoding = Encoding.UTF8;
                    pack_meta = wc.DownloadString(String.Format(META_URL, platform, pack_id));
                }
                return JsonConvert.DeserializeObject<ProductInfoMeta>(pack_meta);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private string GetCurrentPlatform()
        {
            if (cboPlatform.SelectedIndex <= 0)
            {
                return "PC";
            }
            else
            {
                return cboPlatform.Text.Trim().ToLower();
            }
        }

        private bool CheckDownloadFolder(string platform, int pack_id = -1, string pack_name = "")
        {
            try
            {
                var dir = @"Sticker\" + platform;
                // Directory.CreateDirectory(dir);
                if (pack_id > 0)
                {
                    if (String.IsNullOrEmpty(pack_name)) { dir += @"\" + pack_id; }
                    else { dir += @"\" + pack_name; }
                    // Directory.CreateDirectory(dir);
                    if (platform.Equals("iphone"))
                    {
                        dir += @"\2x";
                        // Directory.CreateDirectory(dir);
                    }
                }
                Directory.CreateDirectory(dir);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private string GetDownloadSource(string platform, int pack_id, int sticker_id = -1, bool ani = false, bool x2 = false)
        {
            if (sticker_id > 0)
            {
                if (ani)
                {
                    return String.Format(SOLO_URL_2, platform, pack_id, (x2) ? String.Concat(sticker_id, "@2x") : sticker_id.ToString());
                }
                else
                {
                    return String.Format(SOLO_URL_1, platform, pack_id, (x2) ? String.Concat(sticker_id, "@2x") : sticker_id.ToString());
                }
            }
            else
            {
                if (ani)
                {
                    return String.Format(PACK_URL_2, platform, pack_id, (x2) ? "@2x" : "");
                }
                else
                {
                    return String.Format(PACK_URL_1, platform, pack_id, (x2) ? "@2x" : "");
                }
            }
        }

        private string GetDownloadPath(string platform, int pack_id, string pack_name = "", int sticker_id = -1, bool x2 = false)
        {
            var dir = @"Sticker\" + platform;
            if (String.IsNullOrEmpty(pack_name)) { dir += @"\" + pack_id; }
            else { dir += @"\" + pack_name; }
            if (sticker_id > 0)
            {
                if (x2) { dir += @"\2x"; }
                dir += @"\" + sticker_id;
                if (x2) { dir += "@2x"; }
                dir += ".png";
            }
            else
            {
                if (x2)
                {
                    if (String.IsNullOrEmpty(pack_name)) { dir += "@2x"; }
                    else { dir += " @ 2x"; }
                }
                dir += ".zip";
            }
            return dir;
        }

        private void DownloadSticker(string platform, int pack_id, string pack_name = "", int sticker_id = -1, bool ani = false, bool x2 = false)
        {
            var src = GetDownloadSource(platform, pack_id, sticker_id, ani, x2);
            var dst = GetDownloadPath(platform, pack_id, pack_name, sticker_id, x2);
            using (var wc = new WebClient())
            {
                wc.DownloadFile(src, dst);
            }
        }

        #endregion

        public FrmMain()
        {
            InitializeComponent();
        }

        private void btnGetMeta_Click(object sender, EventArgs e)
        {
            cboName.Items.Clear();
            gbxDownload.Enabled = false;
            var platform = GetCurrentPlatform();
            var pack_id = GetPackageId(txtURL.Text.Trim());
            if (pack_id <= 0)
            {
                MessageBox.Show("Invalid sticker URL detected, please check your input.");
                return;
            }
            var package = GetPackMeta(platform, pack_id);
            if (package == null || package.Title == null || package.Title.Count == 0 /* || package.Stickers == null || package.Stickers.Count == 0 */)
            {
                MessageBox.Show("Cannot get sticker package metadata, possibly invalid sticker ID.");
                return;
            }
            // cboName.DataSource = new BindingSource(package.Title, null);
            // cboName.DisplayMember = "Value";
            // cboName.ValueMember = "Key";
            foreach (DictionaryEntry title in package.Title)
            {
                cboName.Items.Add(String.Format("[{0}] - {1}", title.Key, title.Value));
                if (title.Key.Equals("en"))
                {
                    cboName.SelectedIndex = cboName.Items.Count - 1;
                }
                if (cboName.SelectedIndex < 0)
                {
                    cboName.SelectedIndex = 0;
                }
            }
            lblAnimated.Visible = package.HasAnimation;
            CurrentPackage = package;
            gbxDownload.Enabled = true;
            AcceptButton = btnStart;
            btnStart.Focus();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            cboPlatform.SelectedIndex = 2;
            ActiveControl = txtURL;
            BringToFront();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            var platform = GetCurrentPlatform();
            var package = CurrentPackage;
            var pack_id = CurrentPackage.PackageId;
            var pack_name = "";
            if (chkRename.Checked)
            {
                pack_name = CurrentPackage.Title[cboName.SelectedIndex].ToString();
                var re = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
                var rx = new Regex(String.Format("[{0}]", Regex.Escape(re)));
                pack_name = rx.Replace(pack_name, "_");
            }
            if (!CheckDownloadFolder(platform, pack_id, pack_name))
            {
                MessageBox.Show("Cannot access the Download folder, please check your permissions.");
                return;
            }
            this.Cursor = Cursors.WaitCursor;
            btnStart.Enabled = false;
            // this.lblDownloading.Text = "Downloading Stickers...";
            lblDownloading.Visible = true;
            lblDownloading.BringToFront();
            // this.Enabled = false;
            try
            {
                if (chkSolo.Checked)
                {
                    // var cur = 0;
                    foreach (var sticker in package.Stickers)
                    {
                        // lblDownloading.Text = String.Format("Downloading Stickers... [ {0} / {1} ]", ++cur, package.Stickers.Count);
                        DownloadSticker(platform, pack_id, pack_name, sticker.Id, package.HasAnimation, false);
                        if (platform.Equals("iphone"))
                        {
                            DownloadSticker(platform, pack_id, pack_name, sticker.Id, package.HasAnimation, true);
                        }
                    }
                }
                if (chkPack.Checked)
                {
                    // lblDownloading.Text = "Downloading Sticker Pack...";
                    DownloadSticker(platform, pack_id, pack_name, -1, package.HasAnimation, false);
                    if (platform.Equals("iphone"))
                    {
                        DownloadSticker(platform, pack_id, pack_name, -1, package.HasAnimation, true);
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null) { MessageBox.Show(ex.InnerException.Message); }
                else { MessageBox.Show(ex.Message); }
            }
            // lblDownloading.Text = "";
            lblDownloading.Visible = false;
            lblDownloading.SendToBack();
            btnStart.Enabled = true;
            this.Cursor = Cursors.Default;
            // this.Enabled = true;
            MessageBox.Show("Download completed, please check log for more information.");
            ActiveControl = txtURL;
            txtURL.Clear();
        }

        private void chkRename_CheckedChanged(object sender, EventArgs e)
        {
            cboName.Enabled = chkRename.Checked;
        }

        private void txtURL_TextChanged(object sender, EventArgs e)
        {
            cboName.Items.Clear();
            gbxDownload.Enabled = false;
            AcceptButton = btnGetMeta;
        }

    }
}
