﻿namespace LineStickerDownloader
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.label1 = new System.Windows.Forms.Label();
            this.txtURL = new System.Windows.Forms.TextBox();
            this.btnGetMeta = new System.Windows.Forms.Button();
            this.gbxDownload = new System.Windows.Forms.GroupBox();
            this.chkRename = new System.Windows.Forms.CheckBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.lblAnimated = new System.Windows.Forms.Label();
            this.chkPack = new System.Windows.Forms.CheckBox();
            this.chkSolo = new System.Windows.Forms.CheckBox();
            this.cboName = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboPlatform = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblDownloading = new System.Windows.Forms.Label();
            this.gbxDownload.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Line Store &URL:";
            // 
            // txtURL
            // 
            this.txtURL.Location = new System.Drawing.Point(100, 11);
            this.txtURL.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtURL.Name = "txtURL";
            this.txtURL.Size = new System.Drawing.Size(280, 21);
            this.txtURL.TabIndex = 1;
            this.txtURL.TextChanged += new System.EventHandler(this.txtURL_TextChanged);
            // 
            // btnGetMeta
            // 
            this.btnGetMeta.Location = new System.Drawing.Point(390, 10);
            this.btnGetMeta.Name = "btnGetMeta";
            this.btnGetMeta.Size = new System.Drawing.Size(100, 23);
            this.btnGetMeta.TabIndex = 2;
            this.btnGetMeta.Text = "&Get Metadata";
            this.btnGetMeta.UseVisualStyleBackColor = true;
            this.btnGetMeta.Click += new System.EventHandler(this.btnGetMeta_Click);
            // 
            // gbxDownload
            // 
            this.gbxDownload.Controls.Add(this.chkRename);
            this.gbxDownload.Controls.Add(this.btnStart);
            this.gbxDownload.Controls.Add(this.lblAnimated);
            this.gbxDownload.Controls.Add(this.chkPack);
            this.gbxDownload.Controls.Add(this.chkSolo);
            this.gbxDownload.Controls.Add(this.cboName);
            this.gbxDownload.Controls.Add(this.label2);
            this.gbxDownload.Enabled = false;
            this.gbxDownload.Location = new System.Drawing.Point(10, 40);
            this.gbxDownload.Name = "gbxDownload";
            this.gbxDownload.Size = new System.Drawing.Size(480, 130);
            this.gbxDownload.TabIndex = 3;
            this.gbxDownload.TabStop = false;
            this.gbxDownload.Text = "Download &Options";
            // 
            // chkRename
            // 
            this.chkRename.AutoSize = true;
            this.chkRename.Checked = true;
            this.chkRename.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkRename.Location = new System.Drawing.Point(360, 20);
            this.chkRename.Name = "chkRename";
            this.chkRename.Size = new System.Drawing.Size(108, 17);
            this.chkRename.TabIndex = 8;
            this.chkRename.Text = "&Rename Package";
            this.chkRename.UseVisualStyleBackColor = true;
            this.chkRename.CheckedChanged += new System.EventHandler(this.chkRename_CheckedChanged);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(370, 97);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(100, 23);
            this.btnStart.TabIndex = 7;
            this.btnStart.Text = "Start &Download";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // lblAnimated
            // 
            this.lblAnimated.Location = new System.Drawing.Point(310, 70);
            this.lblAnimated.Name = "lblAnimated";
            this.lblAnimated.Size = new System.Drawing.Size(160, 20);
            this.lblAnimated.TabIndex = 6;
            this.lblAnimated.Text = "Animated stickers available!";
            this.lblAnimated.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblAnimated.Visible = false;
            // 
            // chkPack
            // 
            this.chkPack.AutoSize = true;
            this.chkPack.Checked = true;
            this.chkPack.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkPack.Location = new System.Drawing.Point(145, 98);
            this.chkPack.Name = "chkPack";
            this.chkPack.Size = new System.Drawing.Size(112, 17);
            this.chkPack.TabIndex = 5;
            this.chkPack.Text = "&Packaged Stickers";
            this.chkPack.UseVisualStyleBackColor = true;
            // 
            // chkSolo
            // 
            this.chkSolo.AutoSize = true;
            this.chkSolo.Checked = true;
            this.chkSolo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSolo.Location = new System.Drawing.Point(145, 73);
            this.chkSolo.Name = "chkSolo";
            this.chkSolo.Size = new System.Drawing.Size(112, 17);
            this.chkSolo.TabIndex = 4;
            this.chkSolo.Text = "Individual &Stickers";
            this.chkSolo.UseVisualStyleBackColor = true;
            // 
            // cboName
            // 
            this.cboName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboName.Location = new System.Drawing.Point(140, 40);
            this.cboName.Name = "cboName";
            this.cboName.Size = new System.Drawing.Size(330, 21);
            this.cboName.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(140, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Naming &Language:";
            // 
            // cboPlatform
            // 
            this.cboPlatform.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPlatform.FormattingEnabled = true;
            this.cboPlatform.Items.AddRange(new object[] {
            "PC",
            "Android",
            "iPhone"});
            this.cboPlatform.Location = new System.Drawing.Point(20, 80);
            this.cboPlatform.Name = "cboPlatform";
            this.cboPlatform.Size = new System.Drawing.Size(120, 21);
            this.cboPlatform.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "&Platform:";
            // 
            // lblDownloading
            // 
            this.lblDownloading.BackColor = System.Drawing.SystemColors.Info;
            this.lblDownloading.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDownloading.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDownloading.ForeColor = System.Drawing.SystemColors.InfoText;
            this.lblDownloading.Location = new System.Drawing.Point(0, 0);
            this.lblDownloading.Name = "lblDownloading";
            this.lblDownloading.Size = new System.Drawing.Size(500, 180);
            this.lblDownloading.TabIndex = 4;
            this.lblDownloading.Text = "Downloading Stickers...";
            this.lblDownloading.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblDownloading.Visible = false;
            // 
            // FrmMain
            // 
            this.AcceptButton = this.btnGetMeta;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 180);
            this.Controls.Add(this.cboPlatform);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.gbxDownload);
            this.Controls.Add(this.btnGetMeta);
            this.Controls.Add(this.txtURL);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblDownloading);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Line Sticker Downloader";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.gbxDownload.ResumeLayout(false);
            this.gbxDownload.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtURL;
        private System.Windows.Forms.Button btnGetMeta;
        private System.Windows.Forms.GroupBox gbxDownload;
        private System.Windows.Forms.Label lblAnimated;
        private System.Windows.Forms.CheckBox chkPack;
        private System.Windows.Forms.CheckBox chkSolo;
        private System.Windows.Forms.ComboBox cboName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.ComboBox cboPlatform;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblDownloading;
        private System.Windows.Forms.CheckBox chkRename;
    }
}

